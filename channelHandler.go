package kubernetes

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strings"

	"golang.org/x/crypto/ssh"

	"github.com/containerssh/log"
	"github.com/containerssh/sshserver"
	"github.com/containerssh/unixutils"
	"github.com/containerssh/agent/protocol"
	"github.com/fxamacker/cbor/v2"
)

type x11Session struct {
	SingleConnection   bool
	Protocol           string
	Cookie             string
	reverseConn sshserver.ReverseConnectionHandler
	exec               kubernetesExecution
	x11Chan            ssh.Channel
}

type channelHandler struct {
	channelID             uint64
	networkHandler        *networkHandler
	username              string
	env                   map[string]string
	pty                   bool
	columns               uint32
	rows                  uint32
	x11                   *x11Session
	exec                  kubernetesExecution
	session               sshserver.SessionChannel
	pod                   kubernetesPod
}

func (c *channelHandler) OnUnsupportedChannelRequest(_ uint64, _ string, _ []byte) {
}

func (c *channelHandler) OnFailedDecodeChannelRequest(
	_ uint64,
	_ string,
	_ []byte,
	_ error,
) {
}

func (c *channelHandler) OnEnvRequest(_ uint64, name string, value string) error {
	if c.exec != nil {
		return log.UserMessage(EProgramAlreadyRunning, "program already running", "program already running")
	}
	c.env[name] = value
	return nil
}

func (c *channelHandler) OnPtyRequest(
	_ uint64,
	term string,
	columns uint32,
	rows uint32,
	_ uint32,
	_ uint32,
	_ []byte,
) error {
	if c.exec != nil {
		return log.UserMessage(EProgramAlreadyRunning, "program already running", "program already running")
	}
	c.env["TERM"] = term
	c.pty = true
	c.columns = columns
	c.rows = rows
	return nil
}

func (c *channelHandler) parseProgram(program string) []string {
	programParts, err := unixutils.ParseCMD(program)
	if err != nil {
		return []string{"/bin/sh", "-c", program}
	} else {
		if strings.HasPrefix(programParts[0], "/") || strings.HasPrefix(
			programParts[0],
			"./",
		) || strings.HasPrefix(programParts[0], "../") {
			return programParts
		} else {
			return []string{"/bin/sh", "-c", program}
		}
	}
}

func (c *channelHandler) run(
	ctx context.Context,
	program []string,
) error {
	c.networkHandler.mutex.Lock()
	defer c.networkHandler.mutex.Unlock()

	var err error
	switch c.networkHandler.config.Pod.Mode {
	case ExecutionModeConnection:
		err = c.handleExecModeConnection(ctx, program)
	case ExecutionModeSession:
		c.pod, err = c.handleExecModeSession(ctx, program)
	default:
		// This should never happen due to validation.
		return fmt.Errorf("invalid execution mode: %s", c.networkHandler.config.Pod.Mode)
	}
	if err != nil {
		return err
	}

	c.exec.run(
		c.session.Stdin(),
		c.session.Stdout(),
		c.session.Stderr(),
		c.session.CloseWrite,
		func(exitStatus int) {
			c.session.ExitStatus(uint32(exitStatus))
			if err := c.session.Close(); err != nil && !errors.Is(err, io.EOF) {
				c.networkHandler.logger.Debug(log.Wrap(
					err,
					EFailedOutputCloseWriting,
					"failed to close session",
				))
			}
		},
	)

	if c.pty {
		err = c.exec.resize(ctx, uint(c.rows), uint(c.columns))
		if err != nil {
			c.networkHandler.logger.Debug(log.Wrap(err, EFailedResize, "Failed to set initial terminal size"))
		}
	}

	return nil
}

func (c *channelHandler) handleExecModeConnection(
	ctx context.Context,
	program []string,
) error {
	exec, err := c.networkHandler.pod.createExec(ctx, program, c.env, c.pty)
	if err != nil {
		return err
	}
	c.exec = exec
	return nil
}

func (c *channelHandler) handleExecModeSession(
	ctx context.Context,
	program []string,
) (kubernetesPod, error) {
	pod, err := c.networkHandler.cli.createPod(
		ctx,
		c.networkHandler.labels,
		c.networkHandler.annotations,
		c.env,
		&c.pty,
		program,
	)
	if err != nil {
		return nil, err
	}
	c.exec, err = pod.attach(ctx)
	if err != nil {
		c.removePod(pod)
		return nil, err
	}
	return pod, nil
}

func (c *channelHandler) execX11Forward(
	ctx context.Context,
) (error) {
	if c.networkHandler.config.Pod.Mode == ExecutionModeConnection {
		agent := []string{"/usr/bin/containerssh-agent", "x11-fwd"}
		exec, err := c.networkHandler.pod.createExec(ctx, agent, map[string]string{}, false)
		if err != nil {
			c.networkHandler.logger.Debug(log.NewMessage(MExec, "Failed to start X11"))
			return err
		}
		c.x11.exec = exec

		c.x11Listener()

		return nil
	}

	return fmt.Errorf("Unimplemented")
}

func (c *channelHandler) connectCallback(
	channelId int,
	connectedAddress string,
	connectedPort uint32,
	origAddress string,
	origPort uint32,
) (io.Writer, io.Reader, func(), error) {
	var writer io.Writer
	var reader io.Reader
	ch, err := c.x11.reverseConn.OnX11Request(origAddress, origPort)
	if err != nil {
		return writer, reader, func(){}, err
	}
	return ch, ch, func(){
		c.networkHandler.logger.Debug(log.NewMessage(
			MExec,
			"Closing!",
		))
		ch.Close()
	}, nil
}

func (c *channelHandler) x11Listener() {
	stdin_r, stdin_w := io.Pipe()
	stdout_r, stdout_w := io.Pipe()
	_, stderr_w := io.Pipe()

	c.x11.exec.run(
		stdin_r,
		stdout_w,
		stderr_w,
		func() error {
			c.networkHandler.logger.Debug(log.NewMessage(
				MExec,
				"CloseWrite",
			))
			return nil
		},
		func(exitStatus int) {
			c.networkHandler.logger.Debug(log.NewMessage(
				MExec,
				"Exiting x11",
			))
		},
	)
	handle := protocol.ConnectionHandler{
		SingleConnection:   c.x11.SingleConnection,
		BindHost:           "127.0.0.1",
		BindPort:           "6010",
		Protocol:           "tcp",
		ToAgent:            stdin_w,
		FromAgent:          stdout_r,
		ConnectionCallback: c.connectCallback,
		Logger:             c.networkHandler.logger,
	}

	setup := protocol.SetupPacket{
		Display: "localhost:10",
		AuthProtocol: c.x11.Protocol,
		AuthCookie: c.x11.Cookie,
	}
	mar, err := cbor.Marshal(&setup)
	if err != nil {
		c.networkHandler.logger.Debug(log.NewMessage(
			MExec,
			"Error marshalling setup packet",
		))
		return
	}

	go handle.Listen(mar)
}

func (c *channelHandler) removePod(pod kubernetesPod) {
	ctx, cancelFunc := context.WithTimeout(
		context.Background(), c.networkHandler.config.Timeouts.PodStop,
	)
	defer cancelFunc()
	_ = pod.remove(ctx)
}

func (c *channelHandler) OnExecRequest(
	_ uint64,
	program string,
) error {
	if c.networkHandler.config.Pod.disableCommand {
		return log.UserMessage(
			EProgramExecutionDisabled,
			"Command execution is disabled.",
			"Command execution is disabled.",
		)
	}
	startContext, cancelFunc := context.WithTimeout(
		context.Background(),
		c.networkHandler.config.Timeouts.CommandStart,
	)
	defer cancelFunc()

	return c.run(startContext, c.parseProgram(program))
}

func (c *channelHandler) OnShell(
	_ uint64,
) error {
	startContext, cancelFunc := context.WithTimeout(
		context.Background(),
		c.networkHandler.config.Timeouts.CommandStart,
	)
	defer cancelFunc()

	return c.run(startContext, c.networkHandler.config.Pod.ShellCommand)
}

func (c *channelHandler) OnSubsystem(
	_ uint64,
	subsystem string,
) error {
	startContext, cancelFunc := context.WithTimeout(
		context.Background(),
		c.networkHandler.config.Timeouts.CommandStart,
	)
	defer cancelFunc()

	if binary, ok := c.networkHandler.config.Pod.Subsystems[subsystem]; ok {
		return c.run(startContext, []string{binary})
	}
	return log.UserMessage(ESubsystemNotSupported, "subsystem not supported", "the specified subsystem is not supported (%s)", subsystem)
}

func (c *channelHandler) OnSignal(_ uint64, signal string) error {
	c.networkHandler.mutex.Lock()
	defer c.networkHandler.mutex.Unlock()
	if c.exec == nil {
		return log.UserMessage(
			EProgramNotRunning,
			"Cannot send signal, program is not running.",
			"Cannot send signal, program is not running.",
		)
	}
	ctx, cancelFunc := context.WithTimeout(
		context.Background(),
		c.networkHandler.config.Timeouts.Signal,
	)
	defer cancelFunc()

	return c.exec.signal(ctx, signal)
}

func (c *channelHandler) OnX11(
	_ uint64,
	singleConnection bool,
	protocol string,
	cookie string,
	screen uint32,
	reverseConn sshserver.ReverseConnectionHandler,
) error {
	if c.exec != nil {
		return log.UserMessage(EProgramAlreadyRunning, "program already running", "program already running")
	}
	if c.x11 != nil {
		return log.UserMessage(EProgramAlreadyRunning, "x11 forwarding already requested", "x11 forwarding already requested")
	}
	c.networkHandler.logger.Debug("Setting X11 vars")
	c.env["DISPLAY"] = "localhost:10"
	c.x11 = &x11Session{
		SingleConnection: singleConnection,
		Protocol: protocol,
		Cookie: cookie,
		reverseConn: reverseConn,
	}

	ctx, cancelFunc := context.WithTimeout(
		context.Background(),
		c.networkHandler.config.Timeouts.CommandStart,
	)
	defer cancelFunc()

	err := c.execX11Forward(ctx)
	if err != nil {
			c.networkHandler.logger.Debug(log.Wrap(err, EFailedResize, "X11 error"))
	}
	return err
}

func (c *channelHandler) OnWindow(_ uint64, columns uint32, rows uint32, _ uint32, _ uint32) error {
	c.networkHandler.mutex.Lock()
	defer c.networkHandler.mutex.Unlock()
	if c.exec == nil {
		return log.UserMessage(
			EProgramNotRunning,
			"Cannot resize window, program is not running.",
			"Cannot resize window, program is not running.",
		)
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), c.networkHandler.config.Timeouts.Window)
	defer cancelFunc()

	return c.exec.resize(ctx, uint(rows), uint(columns))
}

func (c *channelHandler) OnClose() {
	if c.exec != nil {
		c.exec.kill()
	}
	pod := c.networkHandler.pod
	if pod != nil && c.networkHandler.config.Pod.Mode == ExecutionModeSession {
		ctx, cancel := context.WithTimeout(
			context.Background(),
			c.networkHandler.config.Timeouts.PodStop,
		)
		defer cancel()
		_ = pod.remove(ctx)
	}
}

func (c *channelHandler) OnShutdown(shutdownContext context.Context) {
	if c.exec != nil {
		c.exec.term(shutdownContext)
		// We wait for the program to exit. This is not needed in session or connection mode, but
		// later we will need to support persistent containers.
		select {
		case <-shutdownContext.Done():
			c.exec.kill()
		case <-c.exec.done():
		}
	}
}
